import "./App.css";
import React from "react";
import { useState } from "react";
import Model from "./components/Model";

function App() {
  const [active, setActive] = useState(0);

  const [toggleSwitch, setToggleSwitch] = useState(false);

  const switchToggle = () => {
    toggleSwitch ? setToggleSwitch(false) : setToggleSwitch(true);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="card">
          <Model activeCard={active} />
          <div className="cardButton">
            <div className="switch">
              <input type={"checkbox"} name="" onClick={switchToggle}></input>

              {toggleSwitch === true && (
                <div className="cardButtonExterior">
                  <h2>Exterior</h2>
                  <div className="exterior_01" onClick={() => setActive(0)} />
                  <div className="exterior_02" onClick={() => setActive(1)} />
                  <div className="exterior_03" onClick={() => setActive(2)} />
                </div>
              )}
              {toggleSwitch === false && (
                <div className="cardButtonInterior">
                  <h2>Interior</h2>
                  <div className="interior_01" onClick={() => setActive(3)} />
                  <div className="interior_02" onClick={() => setActive(4)} />
                  <div className="interior_03" onClick={() => setActive(5)} />
                </div>
              )}
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
