import React from 'react'
import modelo from './modelos.json';

const Model = (props) => {
  return (
    <div className="cardImage">
      <img className='cardImageArq' src={`${modelo[props.activeCard].img}`} alt="casa" />
      <div className='textoCard'>
        <h1>Modelo:{modelo[props.activeCard].nombre}</h1>
        <h2>{modelo[props.activeCard].mat}</h2>
        <div className="cardInfo">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </div>
    </div>
  )
}

export default Model;