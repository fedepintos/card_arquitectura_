import React, { Component } from 'react';

const Modelo = [
    {
        nombre: 1001,
        img: "./images/Test/2.jpg",
    },
    {
        nombre: 1011,
        img:"./images/4010-1.jpg",
    },
    
];


export class CardImage extends Component {
  render() {
    return (
    <>
        <div className="cardImage">
            <img className='cardImageArq' src={Modelo[0].img} alt="casa" />
            <div className='textoCard'>
                <h1>Modelo:{Modelo[0].nombre}</h1>
                <div className="cardInfo">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
        <div className='cardButton'>
            <div className='cardButtonExterior'>
                <p>Exterior</p>
                <div className='exterior_01' />
                <div className='exterior_02' />
                <div className='exterior_03' />
            </div>
            <div className='cardButtonInterior'>
                <p>Interior</p>
                <div className='interior_01' />
                <div className='interior_02' />
                <div className='interior_03' />
            </div>
        </div>
    </>
        
    )
  }
}

export default CardImage;